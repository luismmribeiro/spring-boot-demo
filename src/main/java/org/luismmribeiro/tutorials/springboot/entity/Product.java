package org.luismmribeiro.tutorials.springboot.entity;

/**
 * A dummy example class
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class Product {
	private Long id;
	private String name;
	private String price;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
