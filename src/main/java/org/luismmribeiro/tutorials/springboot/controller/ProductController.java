package org.luismmribeiro.tutorials.springboot.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.luismmribeiro.tutorials.springboot.entity.Product;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The Spring REST controller.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@RestController
@RequestMapping("/products")
public class ProductController {
	private final AtomicLong counter = new AtomicLong();
	private Map<Long, Product> products = new HashMap<>();
	
	@RequestMapping(method = {RequestMethod.GET})
	public Collection<Product> getAll() {
		return products.values();
	}
	
	@RequestMapping(method = {RequestMethod.GET}, 
			value = "/{id}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Product getById(@PathVariable("id") Long id) {
		return products.get(id);
	}
	
	@RequestMapping(method = {RequestMethod.DELETE},
			value = "/{id}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Product deleteById(@PathVariable("id") Long id) {
		return products.remove(id);
	}
	
	@RequestMapping(method = {RequestMethod.POST}, 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public Product create(@RequestBody Product product) {
		if(product == null) {
			return null;
		}
		
		product.setId(counter.incrementAndGet());
		products.put(product.getId(), product);
		
		return product;
	}
	
	@RequestMapping(method = {RequestMethod.PUT}, 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public Product update(@RequestBody Product product) {
		if(product == null || product.getId() == null) {
			return null;
		}
		
		products.put(product.getId(), product);
		return product;
	}
}
