package org.luismmribeiro.tutorials.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring Boot application configuration.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
@SpringBootApplication
@ComponentScan({"org.luismmribeiro.tutorials.springboot.controller"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
